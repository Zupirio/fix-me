import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class udp_echoClient
{
	public static void main(String[] args) throws Exception
	{
		DatagramSocket udpSocket = new DatagramSocket();
		String message = null;
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Please enter a message ('q' to  quit ):");
		while ((message = br.readLine()) != null)
		{
			if (message.equalsIgnoreCase("q"))
			{
				break;
			}
			DatagramPacket packet = udp_echoClient.getPacket(message);
			udpSocket.send(packet);
			udpSocket.receive(packet);
			displayPacketDetails(packet);
			System.out.println("Please enter a message ('q' to  quit ):");
		}
		udpSocket.close();
	}

	public static void displayPacketDetails(DatagramPacket packet)
	{
		byte[] msgBuffer = packet.getData();
		int length = packet.getLength();
		int offSet = packet.getOffset();

		int remotePort = packet.getPort();
		InetAddress remoteAddr = packet.getAddress();
		String message = new String(msgBuffer, offSet, length);

		System.out.println("Server at [IP Address=" + remoteAddr + ", port="
							 + remotePort + ", message=" + message + "]");
	}

	public static DatagramPacket getPacket(String message) throws UnknownHostException
	{
		int PACKET_MAX_LENGTH = 1024;
		byte[] messageBuffer = message.getBytes();
		int length = message.length();

		if (length > PACKET_MAX_LENGTH)
		{
			length = PACKET_MAX_LENGTH;
		}
		DatagramPacket packet = new DatagramPacket(messageBuffer, length);
		InetAddress serverIPAddress = InetAddress.getByName("localhost");
		packet.setAddress(serverIPAddress);
		packet.setPort(159000);
		return packet;
	}

}

