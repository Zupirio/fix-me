import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;

public class nonblockingSocketChannelEchoServer
{
	public static void main(String[] args) throws Exception
	{
		InetAddress hostIP = InetAddress.getByName("localhost");
		int port = 19000;
		Selector selector = Selector.open();
		ServerSocketChannel ssChannel = ServerSocketChannel.open();
		ssChannel.configureBlocking(false);
		ssChannel.bind(new InetSocketAddress(hostIP, port));
		ssChannel.register(selector, SelectionKey.OP_ACCEPT);
		while (true)
		{
			if (selector.select() <= 0)
			{
				continue;
			}
			processReadySet(selector.selectedKeys());
		}
	}

	public static void processReadySet(Set readySet) throws Exception
	{
		Iterator iterator = readySet.iterator();
		while(iterator.hasNext())
		{
			SelectionKey key = (SelectionKey) iterator.next();
			iterator.remove();
			if (key.isAcceptable())
			{
				ServerSocketChannel ssChannel = (ServerSocketChannel) key.channel();
				SocketChannel socketChannel = (SocketChannel) ssChannel.accept();
				socketChannel.configureBlocking(false);
				socketChannel.register(key.selector(), SelectionKey.OP_READ);
			}
			if (key.isReadable())
			{
				String message = processRead(key);
				if (message.length() > 0)
				{
					SocketChannel socketChannel = (SocketChannel) key.channel();
					ByteBuffer buffer = ByteBuffer.wrap(message.getBytes());
					socketChannel.write(buffer);
				}
			}
		}
	}

	public static String processRead(SelectionKey key) throws Exception
	{
		SocketChannel socketChannel = (SocketChannel) key.channel();
		ByteBuffer buffer = ByteBuffer.allocate(1024);
		int bytesCount = socketChannel.read(buffer);
		if (bytesCount > 0)
		{
			buffer.flip();
			return new String(buffer.array());
		}
		return "No message";
	}
}