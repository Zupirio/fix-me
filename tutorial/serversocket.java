import java.net.InetSocketAddress;
import java.net.ServerSocket;

public class serversocket
{
	public static void main(String[] argv) throws Exception
	{
		/* create an unbound server socket */
		ServerSocket serverSocket = new ServerSocket();

		/* create a socket address object */
		InetSocketAddress endPoint = new InetSocketAddress("localhost", 12900);

		/* set the wait queue size to 100 */
		int waitQueueSize = 100;

		serverSocket.bind(endPoint, waitQueueSize);

	}
}